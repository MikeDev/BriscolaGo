package Cards

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

type Deck interface {
	AreDrawnAllCards() bool
	Draw() (*Card, error)
	LastCard() *Card
	Reshuffle() Deck
}

type BriscolaDeck struct {
	Cards   [40]*Card
	Pointer int
}

func NewDummyDeck(cards [40]*Card, pointer int) *BriscolaDeck {
	deck := new(BriscolaDeck)
	deck.Cards = cards
	deck.Pointer = pointer
	return deck
}

func NewPartialDeck(cards []*Card) (*BriscolaDeck, error) {
	if len(cards) > 40 {
		return nil, errors.New(fmt.Sprintf("Cards must be max 40 and you given %d cards\n", len(cards)))
	}
	var deckCards [40]*Card
	for i := 0; i < (40 - len(cards)); i++ {
		deckCards[i] = nil
	}
	for i := (40 - len(cards)); i < 40; i++ {
		deckCards[i] = cards[i-40+len(cards)]
	}
	deck := new(BriscolaDeck)
	deck.Cards = deckCards
	deck.Pointer = 40 - len(cards)
	return deck, nil
}

func NewBriscolaDeck() *BriscolaDeck {
	deck := new(BriscolaDeck)
	for i := 0; i < len(deck.Cards); i++ {
		seed := Seed(i / 10)
		value := i%10 + 1
		deck.Cards[i] = NewCard(seed, (value))
	}
	deck.shuffle()
	deck.Pointer = 0
	return deck
}

func (deck *BriscolaDeck) shuffle() *BriscolaDeck {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(deck.Cards), func(i, j int) {
		deck.Cards[i], deck.Cards[j] = deck.Cards[j], deck.Cards[i]
	})
	return deck
}

func (deck *BriscolaDeck) Reshuffle() Deck {
	deck.shuffle()
	deck.Pointer = 0
	return deck
}

func (deck *BriscolaDeck) AreDrawnAllCards() bool {
	return len(deck.Cards) == deck.Pointer
}

func (deck *BriscolaDeck) Draw() (*Card, error) {
	if deck.AreDrawnAllCards() {
		err := errors.New("Reached end of deck")
		return nil, err
	}
	card := deck.Cards[deck.Pointer]
	deck.Pointer += 1
	return card, nil
}

func (deck *BriscolaDeck) String() string {
	strcards := ""
	for i, card := range deck.Cards {
		if i == deck.Pointer {
			strcards += "-> " + card.String() + "\n"
		} else {
			strcards += card.String() + "\n"
		}
	}
	return fmt.Sprintf("{%s}", strcards)

}
func (deck *BriscolaDeck) LastCard() *Card {
	return deck.Cards[len(deck.Cards)-1]
}
