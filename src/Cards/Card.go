package Cards

import (
	"fmt"
)

func init() {

}

type Seed byte

const (
	Diamonds Seed = 0
	Clubs    Seed = 1
	Spades   Seed = 2
	Hearts   Seed = 3
)

var _strSeed = [4]string{"Diamonds", "Clubs", "Spades", "Hearts"}

func (seed Seed) String() string {
	return _strSeed[seed]
}

type Card struct {
	seed  Seed
	value int
}

func (c *Card) Seed() Seed {
	return c.seed
}

func (c *Card) Value() int {
	return c.value
}

func NewCard(seed Seed, value int) *Card {
	if value < 1 || value > 10 {
		var strerr = fmt.Sprintf("Value must be between 1 and 10. Current Value %d", value)
		panic(strerr)
	}
	card := new(Card)
	card.seed = seed
	card.value = value
	return card
}

func (card *Card) String() string {
	return fmt.Sprintf("(%d, %s)", card.value, card.seed)
}
