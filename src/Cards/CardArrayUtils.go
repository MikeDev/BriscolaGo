package Cards

import (
	"math/rand"
	"time"
)

func RemoveFromCardArrayUnordered(s []*Card, i int) []*Card {
	s[len(s)-1], s[i] = s[i], s[len(s)-1]
	return s[:len(s)-1]
}

func RemoveFromCardArrayOrdered(s []*Card, i int) []*Card {
	return append(s[:i], s[i+1:]...)
}

func FindAndRemoveCard(cards []*Card, c *Card) []*Card {
	var iToRemove = -1
	for i, card := range cards {
		if *card == *c {
			iToRemove = i
			break
		}
	}
	if iToRemove != -1 {
		return RemoveFromCardArrayOrdered(cards, iToRemove)
	} else {
		return cards
	}
}

func MoveCard(from []*Card, to []*Card, i int) ([]*Card, []*Card) {
	cardToMove := from[i]
	from = RemoveFromCardArrayOrdered(from, i)
	to = append(to, cardToMove)
	return from, to

}

func IndexOf(cards []*Card, card *Card) int {
	for i, c := range cards {
		if *c == *card {
			return i
		}
	}
	return -1
}

func ContainsCard(cards []*Card, card *Card) bool {
	return IndexOf(cards, card) != -1
}

func DeepCopySlice(sliceCards []*Card) []*Card {
	deepcopy := make([]*Card, 0, len(sliceCards))
	deepcopy = append(deepcopy, sliceCards...)
	return deepcopy
}

func Shuffle(cards []*Card) []*Card {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(cards), func(i, j int) {
		cards[i], cards[j] = cards[j], cards[i]
	})
	return cards
}

func ShuffleExceptLast(cards []*Card) []*Card {
	Shuffle(cards[:len(cards)-1])
	return cards
}
