package Player

import . "Cards"
import (
	"math/rand"
	"time"
)

type DummyPlayer struct {
	*BasePlayer
}

func (p *DummyPlayer) ChooseCard() int {
	rand.Seed(time.Now().UnixNano())
	sizeHand := len(p.Hand)
	i := rand.Int() % sizeHand
	return i
}

func NewDummyPlayer(hand []*Card, name string) *DummyPlayer {
	p := new(DummyPlayer)
	p.BasePlayer = NewStupidBasePlayer(hand, name)
	p.pIA = p
	return p
}

func NewDummyPlayerWithoutHand(name string) *DummyPlayer {
	return NewDummyPlayer(make([]*Card, 0, 3), name)
}
