package Player

import (
	. "Cards"
	"fmt"
)

var playerID int = 0

type PlayerActions interface {
	AddToHand(card *Card)
	DiscardCard() *Card
	Name() string
	SizeHand() int
	SetGameState(i GameStateI)
	ID() int
}

type GameStateI interface {
	Table() [2]*Card
	BriscolaSeed() Seed
	BriscolaCard() Card
	PointsTable() map[int]int
	Points() [2]int
	Turn() int
	AreAllCardsDrawn() bool
}

type PlayerIA interface {
	ChooseCard() int
}

type stupidIA struct{}

func (stupidIA) ChooseCard() int {
	return 0
}

type BasePlayer struct {
	Hand      []*Card
	pIA       PlayerIA
	name      string
	id        int
	GameState GameStateI
}

func (p *BasePlayer) Name() string {
	return p.name
}

func (p *BasePlayer) SetGameState(gameState GameStateI) {
	p.GameState = gameState
}

func NewStupidBasePlayer(cards []*Card, name string) *BasePlayer {
	return NewBasePlayer(cards, name, stupidIA{})
}

func NewStupidBasePlayerWithoutHand(name string) *BasePlayer {
	return NewStupidBasePlayer(make([]*Card, 0, 3), name)
}

func NewBasePlayer(cards []*Card, name string, IA PlayerIA) *BasePlayer {
	p := new(BasePlayer)
	p.Hand = cards
	p.name = name
	p.pIA = IA
	p.id = playerID
	playerID += 1
	return p
}

func NewBasePlayerWithoutHand(name string, IA PlayerIA) *BasePlayer {
	return NewBasePlayer(make([]*Card, 0, 3), name, IA)
}

func (p *BasePlayer) String() string {
	output := ""
	for i, card := range p.Hand {
		if card == nil {
			output += "nil"
		} else {
			output += card.String()
		}
		if i != len(p.Hand)-1 {
			output += ", "
		}
	}
	return fmt.Sprintf("%s {%s}", p.Name(), output)
}

func (p *BasePlayer) ID() int {
	return p.id
}

func (p *BasePlayer) AddToHand(card *Card) {
	p.Hand = append(p.Hand, card)
}

func (p *BasePlayer) SizeHand() int {
	return len(p.Hand)
}

func (p *BasePlayer) DiscardCard() *Card {
	i := p.pIA.ChooseCard()
	if i < 0 || i > 2 {
		err_message := fmt.Sprintf("Input value not in range [0,2]. Current value given: %d", i)
		panic(err_message)
	}
	card := p.Hand[i]
	p.Hand = RemoveFromCardArrayUnordered(p.Hand, i)
	return card
}
