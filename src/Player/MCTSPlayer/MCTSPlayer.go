package MCTSPlayer

import (
	. "Cards"
	"Game"
	"MCTS"
	"MCTS/MCTS_Phases"
	"Player"
	"fmt"
)

//TODO Load MCTS settings from file
type MCTSPlayer struct {
	*Player.BasePlayer
	unknownCards []*Card
	playedCards  []*Card
	order        int
	NStepsMCTS   int
	nDeckCards   int
}

func (m *MCTSPlayer) SetOrder(i int) {
	m.order = i
}

func NewMCTSPlayer(nStepsMCTS int) *MCTSPlayer {
	mctsP := new(MCTSPlayer)
	mctsP.unknownCards = NewBriscolaDeck().Cards[:]
	mctsP.BasePlayer = Player.NewBasePlayerWithoutHand("MCTSPlayer", mctsP)
	mctsP.NStepsMCTS = nStepsMCTS
	mctsP.nDeckCards = 40
	return mctsP
}

func (m *MCTSPlayer) AddToHand(c *Card) {
	m.unknownCards = FindAndRemoveCard(m.unknownCards, c)
	m.BasePlayer.AddToHand(c)
	m.nDeckCards -= 2
}

func (m *MCTSPlayer) ChooseCard() int {
	fmt.Println("Turn ", m.GameState.Turn())
	if len(m.unknownCards)+len(m.playedCards)+len(m.Hand) != 40 {
		value := len(m.unknownCards) + len(m.playedCards) + len(m.Hand)
		panic(fmt.Sprintf("len(m.unknownCards) + len(m.playedCards) + len(m.Hand) != 40 but is %d", value))
	}
	enemyHSize := 0
	if m.GameState.Turn() <= 17 {
		enemyHSize = 3
	} else if turn := m.GameState.Turn(); turn <= 19 {
		enemyHSize = 19 - turn + 1
	} else {
		panic(fmt.Sprintf("Turn cannot be greater of 19 but is %d", turn))
	}
	if m.order == 1 {
		enemyHSize -= 1
	}
	briscola := m.GameState.BriscolaCard()
	rules := Game.NewBriscolaRules(m.GameState.BriscolaSeed())
	enemyHand, deck := determination(DeepCopySlice(m.unknownCards), &briscola, enemyHSize, m.nDeckCards)
	state := MCTS.NewMatchState(enemyHand, m.playedCards,
		deck, m.Hand, &briscola, m.order == 0, rules,
		m.GameState.Points(),
		m.GameState.Table(), m.order == 1,
		ContainsCard(m.Hand, &briscola) || ContainsCard(m.playedCards, &briscola),
		m.GameState.Turn())
	root := MCTS.NewNode(state, nil)
	root.GenerateSons()
	for i := 0; i < m.NStepsMCTS; i++ {
		MCTS_Phases.AlgorithmStep(root, 10)
	}
	bestNode := MCTS_Phases.SelectBestNodeWithUBC(root.Sons)
	return IndexOf(m.Hand, bestNode.CardObject)
}

func (m *MCTSPlayer) DiscardCard() *Card {
	card := m.BasePlayer.DiscardCard()
	m.playedCards = append(m.playedCards, card)
	return card
}

func (m *MCTSPlayer) ObserveEnemyMove(card *Card) {
	m.playedCards = append(m.playedCards, card)
	m.unknownCards = FindAndRemoveCard(m.unknownCards, card)
}

func determination(unknowCards []*Card, briscolaCard *Card, enemyHandSize, deckSize int) ([]*Card, []*Card) {
	if enemyHandSize+deckSize != len(unknowCards) {
		panic(fmt.Sprintln("enemyHandSize + deckSize != len(unknowCards) - enemyHandSize + deckSize = ", enemyHandSize+deckSize, "len(unknowCards) = ", len(unknowCards)))
	}
	if deckSize > 0 {
		unknowCards = Shuffle(unknowCards)
		oldLen := len(unknowCards)
		unknowCards = FindAndRemoveCard(unknowCards, briscolaCard)
		if len(unknowCards)+1 == oldLen {
			unknowCards = append(unknowCards, briscolaCard)
		}
	}
	enemyHand := DeepCopySlice(unknowCards[:enemyHandSize])
	deck := DeepCopySlice(unknowCards[enemyHandSize:])
	return enemyHand, deck
}
