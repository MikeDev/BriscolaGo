package MCTS

import (
	. "Cards"
	"fmt"
)

func EnemyPlayCard(currentState *MatchState, indexCard int) *MatchState {
	currentState.EnemyHand, currentState.PlayedCards = MoveCard(currentState.EnemyHand, currentState.PlayedCards, indexCard)
	return currentState
}

func MyMove(currentState *MatchState, indexCard int) *MatchState {
	currentState.HandCards, currentState.PlayedCards = MoveCard(currentState.HandCards, currentState.PlayedCards, indexCard)
	return currentState
}

func FirstPlayerMove(state *MatchState, card *Card) *MatchState {
	if state.SecondTurn {
		panic("Should be second turn")
	}
	state.SecondTurn = true
	state.Table = [2]*Card{card, nil}
	return state
}

func SecondPlayerMove(state *MatchState, card *Card) *MatchState {
	if !state.SecondTurn {
		panic("Should be first turn")
	}
	state.SecondTurn = false
	state.Table[1] = card
	state = winnerAndUpdateVariables(state)
	return state
}

func MakeMove(state *MatchState, card *Card) *MatchState {
	//fmt.Println("Second turn: ", state.SecondTurn)
	if !state.SecondTurn {
		return FirstPlayerMove(state, card)
	} else {
		return SecondPlayerMove(state, card)
	}
}

func winnerAndUpdateVariables(state *MatchState) *MatchState {
	state.Turn += 1
	winner := state.Rules.GetTurnWinner(&state.Table)
	if state.IAmTheFirst {
		state.IAmTheFirst = winner == 0
	} else {
		state.IAmTheFirst = winner == 1
	}
	for _, card := range state.Table {
		if cardValue, hasValue := (*state.Rules.CardValueMap)[card.Value()]; hasValue {
			state.Points[winner] += cardValue
		}
	}
	if len(state.DeckCards) > 0 {
		if state.IAmTheFirst {
			state.HandCards = append(state.HandCards, state.DeckCards[0])
			state.EnemyHand = append(state.EnemyHand, state.DeckCards[1])
		} else {
			state.EnemyHand = append(state.EnemyHand, state.DeckCards[0])
			state.HandCards = append(state.HandCards, state.DeckCards[1])
		}
		state.DeckCards = state.DeckCards[2:]
		if len(state.DeckCards)%2 == 1 {
			panic(fmt.Sprintln("Deck len is dispair"))
		}
	}
	return state
}
