package MCTS

import (
	. "Cards"
)

type Node struct {
	Father     *Node
	Sons       []*Node
	NWins      int
	NVisit     int
	state      *MatchState
	CardObject *Card
}

func NewNode(initState *MatchState, father *Node) *Node {
	node := new(Node)
	node.Father = father
	node.NVisit = 0
	node.NWins = 0
	node.state = initState
	node.CardObject = nil
	return node
}

func (n *Node) generateMovesFromCardSlice(cardSlice []*Card, moveGenerator func(*MatchState, int) *MatchState) []*Node {
	sons := make([]*Node, 0, len(cardSlice))
	for i, card := range cardSlice {
		sonState := DeepCopyMatchState(n.state)
		son := NewNode(moveGenerator(sonState, i), n)
		son.CardObject = card
		son.state = MakeMove(son.state, card)
		sons = append(sons, son)
	}
	return sons
}

func (n *Node) GenerateSons() {
	enemyMoves := n.state.IAmTheFirst && n.state.SecondTurn || !n.state.IAmTheFirst && !n.state.SecondTurn
	myMoves := n.state.IAmTheFirst && !n.state.SecondTurn || !n.state.IAmTheFirst && n.state.SecondTurn
	if enemyMoves == myMoves {
		panic("enemyMoves and myMoves must be have different value")
	} else if enemyMoves {
		n.Sons = n.generateMovesFromCardSlice(n.state.EnemyHand, EnemyPlayCard)
	} else if myMoves {
		n.Sons = n.generateMovesFromCardSlice(n.state.HandCards, MyMove)
	}
}

func (n *Node) State() *MatchState {
	return n.state
}
