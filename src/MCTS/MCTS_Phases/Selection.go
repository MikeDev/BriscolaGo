package MCTS_Phases

import (
	. "MCTS"
	"math"
)

var totalExploration int = 0

func Selection(root *Node) *Node {
	leafes := getLeafes(root)
	leafesN := make([]*Node, 0, len(leafes))
	for _, leaf := range leafes {
		if !isTerminal(leaf) {
			leafesN = append(leafesN, leaf)
		}
	}
	if len(leafesN) > 0 {
		return SelectBestNodeWithUBC(leafesN)
	} else {
		return nil
	}
}
func isTerminal(node *Node) bool {
	return node.State().Turn >= 19
}

func getLeafes(root *Node) []*Node {
	leafes := make([]*Node, 0, 1)
	if root.Sons != nil {
		for _, son := range root.Sons {
			leafes = append(leafes, getLeafes(son)...)
		}
	} else {
		leafes = append(leafes, root)
	}
	return leafes
}

func SelectBestNode(leafes []*Node, evaluator func(*Node) float64) *Node {
	var bestNode *Node = nil
	bestValue := 0.0
	for _, leaf := range leafes {
		valNode := evaluator(leaf)
		if valNode > bestValue {
			bestNode = leaf
			bestValue = valNode
		}
	}
	return bestNode
}

func uBC(node *Node) float64 {
	nVisits := node.NVisit
	nWins := node.NWins
	if nWins == 0 {
		return math.Inf(1)
	}
	exploitation := float64(nWins) / float64(nVisits)
	exploration := math.Sqrt(2) * math.Sqrt(math.Log(float64(totalExploration))/float64(nVisits))
	return exploitation + exploration
}

func SelectBestNodeWithUBC(leafes []*Node) *Node {
	return SelectBestNode(leafes, uBC)
}
