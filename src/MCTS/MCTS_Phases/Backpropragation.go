package MCTS_Phases

import . "MCTS"

func Backpropagation(simulated *Node, winsInc int, visitsInc int) {
	if simulated.Father != nil {
		backpropagationInc(simulated.Father, winsInc, visitsInc)
	}
}

func backpropagationInc(father *Node, winsInc int, visitsInc int) {
	father.NWins += winsInc
	father.NVisit += visitsInc
	if father.Father != nil {
		backpropagationInc(father.Father, winsInc, visitsInc)
	}
}
