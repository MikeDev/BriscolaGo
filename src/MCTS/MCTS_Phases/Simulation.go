package MCTS_Phases

import (
	. "MCTS"
	. "MCTS/BriscolaSimulation"
)

func Simulate(node *Node, nSimulations int) (int, int) {
	nodestate := node.State()
	simulation := NewSimulation(nodestate)
	wins, loses := simulation.Simulate(nSimulations)
	node.NWins += wins
	node.NVisit += 1
	return wins, loses
}
