package MCTS_Phases

import (
	. "MCTS"
)

func AlgorithmStep(root *Node, nsimul int) {
	bestNode := Selection(root)
	if bestNode == nil {
		return
	}
	if bestNode.NVisit != 0 {
		Explore(bestNode)
		bestNode = bestNode.Sons[0]
	}
	wins, _ := Simulate(bestNode, nsimul)
	totalExploration += 1
	Backpropagation(bestNode, wins, 1)
}
