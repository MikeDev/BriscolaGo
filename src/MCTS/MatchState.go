package MCTS

import (
	. "Cards"
	"Game"
	"Player"
)

type MatchState struct {
	EnemyHand      []*Card
	DeckCards      []*Card
	PlayedCards    []*Card
	HandCards      []*Card
	BriscolaCard   *Card
	IAmTheFirst    bool
	Rules          *Game.BriscolaRules
	MyGameState    Player.GameStateI
	EnemyGameState Player.GameStateI
	BriscolaDrawn  bool
	Points         [2]int
	SecondTurn     bool
	Table          [2]*Card
	Turn           int
}

func NewMatchState(enemyHand, playedCards, deckCards, handCards []*Card,
	briscolaCard *Card, IAmTheFirst bool,
	rules *Game.BriscolaRules,
	points [2]int, table [2]*Card, secondTurn,
	briscolaDrawn bool, turn int) *MatchState {
	mState := new(MatchState)
	mState.EnemyHand = DeepCopySlice(enemyHand)
	mState.PlayedCards = DeepCopySlice(playedCards)
	mState.DeckCards = DeepCopySlice(deckCards)
	mState.HandCards = DeepCopySlice(handCards)
	mState.BriscolaCard = briscolaCard
	mState.IAmTheFirst = IAmTheFirst
	mState.Rules = rules
	mState.Points = points
	mState.SecondTurn = secondTurn
	mState.Table = table
	mState.BriscolaDrawn = briscolaDrawn
	mState.Turn = turn
	return mState
}

func DeepCopyMatchState(state *MatchState) *MatchState {
	return NewMatchState(state.EnemyHand,
		state.PlayedCards, state.DeckCards, state.HandCards,
		state.BriscolaCard, state.IAmTheFirst,
		state.Rules, state.Points,
		state.Table, state.SecondTurn,
		state.BriscolaDrawn, state.Turn)
}
