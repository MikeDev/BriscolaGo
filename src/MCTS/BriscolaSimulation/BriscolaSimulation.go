package BriscolaSimulation

import (
	. "Cards"
	"Game"
	. "MCTS"
	"Player"
	"fmt"
	"runtime"
)

type Simulation struct {
	state *MatchState
	Wins  int
	Loses int
}

func NewSimulation(state *MatchState) *Simulation {
	simulation := new(Simulation)
	simulation.Loses, simulation.Wins = 0, 0
	simulation.state = DeepCopyMatchState(state)
	return simulation

}

func (s *Simulation) initGameManager(start *MatchState) *Game.GameManager {
	myPlayer, enemyPlayer := Player.NewDummyPlayer(start.HandCards, "MyPlayer"),
		Player.NewDummyPlayer(start.EnemyHand, "EnemyPlayer")
	firstPlayer, secondPlayer := myPlayer, enemyPlayer
	if !start.IAmTheFirst {
		firstPlayer, secondPlayer = enemyPlayer, myPlayer
	}
	deck, err := NewPartialDeck(start.DeckCards)
	if err != nil {
		panic(err.Error())
	}
	return Game.NewSimulatedGame(firstPlayer, secondPlayer, deck,
		start.BriscolaCard, start.Points,
		start.SecondTurn, start.Table, start.Turn)
}

func (s *Simulation) simulateOne(chanWins chan int) {
	gameManager := s.initGameManager(DeepCopyMatchState(s.state))
	if !s.state.SecondTurn && len(s.state.EnemyHand) != len(s.state.HandCards) {
		panic(fmt.Sprintln("Not second turn and players have diffrent hand size -  MyPlayer: ",
			len(s.state.HandCards), " Enemy Player: ", len(s.state.EnemyHand)))
	}
	if len(s.state.DeckCards)%2 == 1 {
		panic("deck size must be pair")
	}
	winnerPlayer := gameManager.PlayGame()
	if winnerPlayer != nil {
		if winnerPlayer.Name() == "MyPlayer" {
			chanWins <- 1
		} else {
			chanWins <- -1
		}
	} else {
		chanWins <- 0
	}
}

func (s *Simulation) Simulate(n int) (int, int) {
	winsChan := make(chan int, n/4)
	numthreads := runtime.NumCPU() * 2
	for i := 0; i < numthreads; i++ {
		go func(i, n int) {
			start, end := i*n/numthreads, (i+1)*n/numthreads
			for j := start; j < end; j++ {
				s.simulateOne(winsChan)
			}
		}(i, n)
	}
	for i := 0; i < n; i++ {
		result := <-winsChan
		if result == -1 {
			s.Loses += 1
		} else if result == 1 {
			s.Wins += 1
		}
	}
	close(winsChan)
	return s.Wins, s.Loses
}
