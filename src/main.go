package main

import (
	"Game"
	"Player"
	"Player/MCTSPlayer"
	"fmt"
)

func main() {
	winp1, winp2 := DONMatches(1000)
	fmt.Printf("MCTS wins %d matches - Random player wins %d matches", winp1, winp2)
}

func DONMatches(n int) (int, int) {
	winp1, winp2 := 0, 0
	for i := 0; i < n; i++ {
		p1 := MCTSPlayer.NewMCTSPlayer(15)
		p2 := Player.NewDummyPlayerWithoutHand("Noob")
		game := Game.NewBriscolaGame(p1, p2)
		game.AppendEnemyObserver(p1)
		game.AppendOrderObserver(p1)
		winner := game.InitGameAndPlay()
		if winner != nil {
			fmt.Println("Winner is ", winner.Name())
			if winner.Name() == "Noob" {
				winp2 += 1
			} else {
				winp1 += 1
			}
		} else {
			fmt.Println("Draw")
		}
	}
	return winp1, winp2
}
