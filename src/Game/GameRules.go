package Game

import (
	. "Cards"
)

type GameRules interface {
	GetTurnWinner(table *[2]*Card) int
	IsFinish(deck *BriscolaDeck, handP1, handP2 [3]*Card) bool
	GetWinner(pointScores [2]int) Result
}

type BriscolaRules struct {
	CardValueMap *map[int]int
	briscolaSeed Seed
}

func NewBriscolaRules(briscolaSeed Seed) *BriscolaRules {
	rules := new(BriscolaRules)
	rules.briscolaSeed = briscolaSeed
	rules.CardValueMap = &(map[int]int{8: 2, 9: 3, 10: 4, 3: 10, 1: 11})
	return rules
}

func (b *BriscolaRules) GetTurnWinner(table *[2]*Card) int {
	card0, card1 := table[0], table[1]
	card0Value := card0.Value()
	card1Value := card1.Value()
	pointsTable := *b.CardValueMap
	if card0Points, contains0 := pointsTable[card0.Value()]; contains0 {
		card0Value += card0Points * 2
	}
	if card1Points, contains1 := pointsTable[card1.Value()]; contains1 {
		card1Value += card1Points * 2
	}
	if card0.Seed() == b.briscolaSeed {
		card0Value += 100
	}
	if card1.Seed() == b.briscolaSeed {
		card1Value += 100
	} else if card1.Seed() != card0.Seed() {
		card1Value -= 1000
	}
	if card1Value > card0Value {
		return 1
	} else {
		return 0
	}
}

func (b *BriscolaRules) IsFinish(deck Deck, sizeHandP1, sizeHandP2 int, points [2]int) bool {
	if points[0] > 60 || points[1] > 60 {
		return true
	}
	if deck.AreDrawnAllCards() {
		return sizeHandP1 == 0 && sizeHandP2 == 0
	}
	return false
}

func (b *BriscolaRules) GetWinner(pointScores [2]int) Result {
	if pointScores[0] > pointScores[1] {
		return WINP1
	} else if pointScores[1] > pointScores[0] {
		return WINP2
	} else {
		return DRAW
	}

}
