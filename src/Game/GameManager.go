package Game

import (
	. "Cards"
	"Player"
)

type GameManager struct {
	GS      *GameState
	deck    Deck
	players [2]Player.PlayerActions
	BriscolaRules
	secondTurn bool
	*Notifier
}

func NewBriscolaGame(p1, p2 Player.PlayerActions) *GameManager {
	gm := new(GameManager)
	gm.players = [2]Player.PlayerActions{p1, p2}
	gm.deck = NewBriscolaDeck()
	gm.GS = NewGameState(gm.deck.LastCard(), gm.deck)
	p1.SetGameState(gm.GS)
	p2.SetGameState(gm.GS)
	gm.BriscolaRules = BriscolaRules{CardValueMap: &gm.GS.pointsTable, briscolaSeed: gm.GS.briscolaSeed}
	gm.Notifier = newGameNotifier()
	gm.secondTurn = false
	return gm
}

func NewSimulatedGame(p1, p2 Player.PlayerActions, deck Deck, briscola *Card, points [2]int, secondTurn bool, table [2]*Card, turn int) *GameManager {
	gm := new(GameManager)
	gm.players = [2]Player.PlayerActions{p1, p2}
	gm.deck = deck
	gm.GS = NewGameState(briscola, gm.deck)
	gm.GS.points = &points
	p1.SetGameState(gm.GS)
	p2.SetGameState(gm.GS)
	gm.BriscolaRules = BriscolaRules{CardValueMap: &gm.GS.pointsTable, briscolaSeed: gm.GS.briscolaSeed}
	gm.secondTurn = secondTurn
	gm.Notifier = newGameNotifier()
	gm.GS.table = &table
	gm.GS.turn = turn
	return gm
}

func (g *GameManager) DrawPhase() {
	for i := 0; i < 2; i++ {
		cardDrawn, err := g.deck.Draw()
		if err == nil {
			g.players[i].AddToHand(cardDrawn)
		}
	}
}

func (g *GameManager) initGame() {
	g.deck.Reshuffle()
	for _, player := range g.players {
		for i := 0; i < 3; i++ {
			card, err := g.deck.Draw()
			if err != nil {
				panic(err)
			}
			player.AddToHand(card)
		}
	}
}

func (g *GameManager) PlayTurn() Result {
	if g.IsFinish(g.deck, g.players[0].SizeHand(), g.players[1].SizeHand(), g.GS.Points()) {
		return g.GetWinner(*g.GS.points)
	}
	cardP1 := g.PlayTurnP1Throw()
	cardP2 := g.PlayTurnP2Throw()
	g.GetWinnerAndUpdateState(cardP1, cardP2)
	g.DrawPhase()
	return NONE
}

func (g *GameManager) GetWinnerAndUpdateState(cardP1 *Card, cardP2 *Card) {
	turnWinner := g.GetTurnWinner(g.GS.table)
	g.increasePointsWinner(cardP1, cardP2, turnWinner)
	g.changeOrder(turnWinner)
	g.GS.turn += 1
}

func (g *GameManager) PlayTurnP2Throw() *Card {
	cardP2 := g.players[1].DiscardCard()
	g.GS.table[1] = cardP2
	g.notifyEnemyObserver(cardP2, g.players[0].ID())
	return cardP2
}

func (g *GameManager) PlayTurnP1Throw() *Card {
	var cardP1 *Card
	if g.secondTurn {
		cardP1 = g.GS.table[0]
		g.secondTurn = false
	} else {
		cardP1 = g.players[0].DiscardCard()
		g.GS.table[0] = cardP1
		g.notifyEnemyObserver(cardP1, g.players[1].ID())
	}
	return cardP1
}

func (g *GameManager) changeOrder(turnWinner int) {
	g.players[0], g.players[1] = g.players[turnWinner], g.players[1-turnWinner]
	g.GS.points[0], g.GS.points[1] = g.GS.points[turnWinner], g.GS.points[1-turnWinner]
	g.notifyOrderObserver(0, g.players[0].ID())
	g.notifyOrderObserver(1, g.players[1].ID())
}

func (g *GameManager) increasePointsWinner(cardP1 *Card, cardP2 *Card, turnWinner int) {
	gainedValue := 0
	if valueCardP1, hasValue := g.GS.pointsTable[cardP1.Value()]; hasValue {
		gainedValue += valueCardP1
	}
	if valueCardP2, hasValue := g.GS.pointsTable[cardP2.Value()]; hasValue {
		gainedValue += valueCardP2
	}
	g.GS.points[turnWinner] += gainedValue
}

func (g *GameManager) InitGameAndPlay() Player.PlayerActions {
	g.initGame()
	return g.PlayGame()
}

func (g *GameManager) PlayGame() Player.PlayerActions {
	var result Result = NONE
	for result == NONE {
		result = g.PlayTurn()
	}
	var playerWinner Player.PlayerActions = nil
	if result == WINP1 {
		playerWinner = g.players[0]
	} else if result == WINP2 {
		playerWinner = g.players[1]
	}
	return playerWinner
}
