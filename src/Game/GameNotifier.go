package Game

import . "Cards"

type EnemyObserver interface {
	ObserveEnemyMove(card *Card)
	ID() int
}

type OrderObserver interface {
	SetOrder(i int)
	ID() int
}

type Notifier struct {
	enemyObservers []EnemyObserver
	orderObservers []OrderObserver
}

func newGameNotifier() *Notifier {
	n := new(Notifier)
	n.enemyObservers = make([]EnemyObserver, 0, 2)
	return n
}

func (n *Notifier) AppendEnemyObserver(e EnemyObserver) {
	n.enemyObservers = append(n.enemyObservers, e)
}

func (n *Notifier) AppendOrderObserver(o OrderObserver) {
	n.orderObservers = append(n.orderObservers, o)
}

func (n *Notifier) notifyEnemyObserver(card *Card, enemyID int) {
	for _, observer := range n.enemyObservers {
		if observer.ID() == enemyID {
			observer.ObserveEnemyMove(card)
		}
	}
}

func (n *Notifier) notifyOrderObserver(order int, enemyID int) {
	for _, observer := range n.orderObservers {
		if observer.ID() == enemyID {
			observer.SetOrder(order)
		}
	}
}
