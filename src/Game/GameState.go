package Game

import (
	. "Cards"
)

type Result int

const (
	NONE  Result = -1
	WINP1 Result = 0
	WINP2 Result = 1
	DRAW  Result = 2
)

type GameState struct {
	table        *[2]*Card
	briscolaSeed Seed
	briscolaCard *Card
	pointsTable  map[int]int
	points       *[2]int
	turn         int
	deck         Deck
}

func (g *GameState) Table() [2]*Card {
	return *g.table
}

func (g *GameState) BriscolaSeed() Seed {
	return g.briscolaSeed
}

func (g *GameState) BriscolaCard() Card {
	return *g.briscolaCard
}

func (g *GameState) PointsTable() map[int]int {
	return g.pointsTable
}

func (g *GameState) Points() [2]int {
	return *g.points
}

func (g *GameState) Turn() int {
	return g.turn
}

func (g *GameState) AreAllCardsDrawn() bool {
	return g.deck.AreDrawnAllCards()
}

func NewGameState(briscola *Card, deck Deck) *GameState {
	gs := new(GameState)
	gs.pointsTable = map[int]int{8: 2, 9: 3, 10: 4, 3: 10, 1: 11}
	gs.table = new([2]*Card)
	gs.briscolaCard = briscola
	gs.briscolaSeed = briscola.Seed()
	gs.points = new([2]int)
	gs.points[0] = 0
	gs.points[1] = 0
	gs.deck = deck
	return gs
}
