package test

import (
	. "Cards"
	"testing"
)

func TestRemoveFromCardArrayUnordered(t *testing.T) {
	cards := []*Card{NewCard(Spades, 3), NewCard(Hearts, 9)}
	expected := []*Card{NewCard(Hearts, 9)}
	actual := RemoveFromCardArrayUnordered(cards, 0)
	if len(actual) != 1 {
		t.Errorf("Error on card remotion. Actual array  %s- Expected %s", actual, expected)
	}
	if *actual[0] != *expected[0] {
		t.Errorf("Error on equality. Actual array  %s- Expected %s", actual, expected)
	}
}

func TestRemoveFromCardArrayOrdered(t *testing.T) {
	cards := []*Card{NewCard(Spades, 3), NewCard(Hearts, 9), NewCard(Clubs, 3)}
	expected := []*Card{NewCard(Spades, 3), NewCard(Clubs, 3)}
	actual := RemoveFromCardArrayOrdered(cards, 1)
	if len(actual) != 2 {
		t.Errorf("Error on card remotion. Actual array  %s- Expected %s", actual, expected)
	}
	for i := range actual {
		if *actual[i] != *expected[i] {
			t.Errorf("Error on equality. Actual array  %s- Expected %s", actual, expected)
		}
	}
}

func TestFindAndRemoveCard(t *testing.T) {
	cards := []*Card{NewCard(Spades, 3), NewCard(Hearts, 9), NewCard(Hearts, 5)}
	cards = FindAndRemoveCard(cards, NewCard(Hearts, 9))
	if len(cards) != 2 {
		t.Errorf("Error on removing %s - len of cards array is not 2 but %d",
			NewCard(Hearts, 9), len(cards))
	}
	cards = FindAndRemoveCard(cards, NewCard(Spades, 3))
	if len(cards) != 1 {
		t.Errorf("Error on removing %s - len of cards array is not 2 but %d",
			NewCard(Spades, 3), len(cards))
	}
	if *cards[0] != *NewCard(Hearts, 5) {
		t.Errorf("Last card is not %s but %s",
			NewCard(Hearts, 5), cards[0])
	}
}
